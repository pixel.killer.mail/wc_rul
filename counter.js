class Counter {
  constructor(){
    this.label = document.querySelector('component-label')
    this.labelValue = 0
    console.log ('hola')
    document.addEventListener ('-', this.substract.bind(this))
    document.addEventListener ('+', this.add.bind(this))
  }

  substract(){
    this.labelValue -= 1
    this.label.setAttribute('value', this.labelValue)
  }

  add(){
    this.labelValue += 1
    this.label.setAttribute('value', this.labelValue)
  }
}

new Counter