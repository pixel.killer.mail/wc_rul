const labelTemplate = document.createElement('template')

labelTemplate.innerHTML = 
  `

    <label></label>
  `

class componentLabel extends HTMLElement{
  constructor (){
    super()
    this.root = this.attachShadow({mode: 'open'})
    this.root.appendChild(labelTemplate.content.cloneNode(true))
  }

  static get observedAttributes(){
    return ['value']
  }

  attributeChangedCallback(name, _oldVal, newVal){
    this.root.querySelector('label').innerHTML = newVal
  }
}

window.customElements.define('component-label', componentLabel)